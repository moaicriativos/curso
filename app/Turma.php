<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Turma extends Model
{
    protected $table = 'turma';
    protected $primaryKey = 'id';
    use SoftDeletes;
}
