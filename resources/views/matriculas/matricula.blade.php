@extends('layouts.app')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">


        <div class="row m-b-lg m-t-lg">
            <div class="col-md-6">

                <div class="profile-image">
                    <img src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png" class="rounded-circle circle-border m-b-md" alt="profile">
                </div>
                <div class="profile-info">
                    <div class="">
                        <div>
                            <h2 class="no-margins">
                                {{$matricula->nome}}
                            </h2>
                            <h4>{{$matricula->email}}</h4>
                            <small>
{{--                                There are many variations of passages of Lorem Ipsum available, but the majority--}}
{{--                                have suffered alteration in some form Ipsum available.--}}
                            </small>
{{--                            {{dd($matricula)}}--}}
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="row">

            <div class="col-lg-4">

                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Dados Pessoais</h3>
                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">CPF: </span>{{ $matricula->cpf }}</span></p>
                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">E-mail: </span>{{ $matricula->email }}</span></p>
                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">Data de nascimento: </span>{{ $matricula->datanascimento }}</span></p>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Dados do responsável</h3>


                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">Nome: </span>{{ $matricula->nomeresponsavel }}</span></p>
                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">CPF: </span>{{ $matricula->cpfresponsavel }}</span></p>
                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">RG: </span>{{ $matricula->rgresponsavel }}</span></p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Filiação</h3>


                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">Nome da mãe: </span>{{ $matricula->nomemae }}</span></p>
                        <p><span><i class="fa fa-circle text-black-50"></i> <span class="font-bold">Nome do pai: </span>{{ $matricula->nomepai }}</span></p>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
